package com.haladyj.csrfpostman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfPostmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrfPostmanApplication.class, args);
	}

}
